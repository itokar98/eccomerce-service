from django.contrib.auth.models import User
from django.db import models
from django.db.models.deletion import CASCADE

from ecommerce_rest_api.models.product import ProductModel

class CategoryModel(models.Model):
    name: models.CharField = models.CharField(max_length=100, blank=True)
    prdoucts: models.ManyToManyField = models.ManyToManyField(ProductModel, blank=True)

    def __str__(self):
        return self.name
