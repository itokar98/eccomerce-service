from django.db import models

class ProductModel(models.Model):
    name: models.CharField = models.CharField(max_length=100, blank=True)
    description: models.TextField = models.TextField()
    image: models.URLField = models.URLField()
    price: models.FloatField = models.FloatField()

    def __str__(self) -> str:
        return self.name

    # class Meta:
    #     app_label = 'myapp'
