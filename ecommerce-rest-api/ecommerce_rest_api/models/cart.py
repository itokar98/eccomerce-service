from django.contrib.auth.models import User
from django.db import models
from django.db.models.deletion import CASCADE

from ecommerce_rest_api.models.product import ProductModel

class CartItemModel(models.Model):
    product: models.OneToOneField = models.OneToOneField(ProductModel, on_delete=CASCADE)
    count: models.IntegerField = models.IntegerField(default=0)

    def __str__(self):
        return f'Product: {self.product}, count: {self.count}'

class CartModel(models.Model):
    user: models.OneToOneField = models.OneToOneField(User, on_delete=CASCADE)
    cart_items: models.ManyToManyField = models.ManyToManyField(CartItemModel, blank=True)


