from django.contrib.auth.models import User
from django.db import models
from django.db.models.aggregates import Count
from django.db.models.deletion import CASCADE

from ecommerce_rest_api.models.product import ProductModel

class OrderItemModel(models.Model):
    product: models.ForeignKey = models.ForeignKey(to=ProductModel, on_delete=CASCADE)
    count: models.ImageField = models.IntegerField()

class OrderModel(models.Model):
    user_id = models.IntegerField()
    prdoucts: models.ManyToManyField = models.ManyToManyField(ProductModel)

    def __str__(self) -> str:
        return f'Order: {self.pk}'
