from rest_framework import serializers
from ecommerce_rest_api.models.cart import CartModel, CartItemModel

class CartItemSerializer(serializers.ModelSerializer):
    """Serializer for CartModel."""

    class Meta:
        model = CartItemModel
        fields = '__all__'

class CartSerializer(serializers.ModelSerializer):
    """Serializer for CartModel."""

    class Meta:
        model = CartModel
        fields = ['id', 'cart_items']
        depth = 2
