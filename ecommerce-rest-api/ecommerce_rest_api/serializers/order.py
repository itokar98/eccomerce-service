from rest_framework import serializers
from ecommerce_rest_api.models.order import OrderModel


class OrderSerializer(serializers.ModelSerializer):
    """Serializer for CobaltModel."""

    class Meta:
        model = OrderModel
        fields = '__all__'
