from rest_framework import serializers
from ecommerce_rest_api.models.product import ProductModel


class ProductSerializer(serializers.ModelSerializer):
    """Serializer for CobaltModel."""

    class Meta:
        model = ProductModel
        fields = '__all__'
