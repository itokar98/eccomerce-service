from ecommerce_rest_api.models.catalog import CatalogModel
from ecommerce_rest_api.models.product import ProductModel
from ecommerce_rest_api.models.cart import CartModel, CartItemModel
from ecommerce_rest_api.models.catalog import CatalogModel
from ecommerce_rest_api.models.order import OrderModel
from django.contrib import admin

# Register your models here.

admin.site.register(ProductModel)
admin.site.register(CartModel)
admin.site.register(CartItemModel)
admin.site.register(CatalogModel)
admin.site.register(OrderModel)
