from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from ecommerce_rest_api.models.order import OrderModel, OrderItemModel
from ecommerce_rest_api.serializers.order import OrderSerializer


class OrderViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving users.
    """

    queryset = OrderModel.objects.all()
    serializer_class = OrderSerializer

