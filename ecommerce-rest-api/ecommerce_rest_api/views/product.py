from ecommerce_rest_api.models.product import ProductModel
from ecommerce_rest_api.serializers.product import ProductSerializer
from ecommerce_rest_api.views.hateoas import HateoasModelViewSet


class ProductViewSet(HateoasModelViewSet):
    """
    A simple ViewSet for listing or retrieving users.
    """

    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer

    def get_list_links(self, request):
        return {
            'self': {'href': request.build_absolute_uri(request.path)},
        }
