from django.contrib.auth.models import User
from ecommerce_rest_api.serializers.user import UserSerializer, PasswordSerializer
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response



class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A simple ViewSet for listing or retrieving users.
    """

    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(detail=True, methods=['get'])
    def set_password(self, request, pk=None):
        return Response({'status': 'password set'})
