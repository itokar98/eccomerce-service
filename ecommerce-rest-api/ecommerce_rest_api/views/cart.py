from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from ecommerce_rest_api.models.cart import CartModel, CartItemModel
from ecommerce_rest_api.models.order import OrderItemModel, OrderModel
from ecommerce_rest_api.models.product import ProductModel
from ecommerce_rest_api.serializers.cart import CartSerializer
from ecommerce_rest_api.serializers.order import OrderSerializer
import json


class CartViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving cart.
    """

    queryset = CartModel.objects.all()
    serializer_class = CartSerializer
    permission_classes = [IsAuthenticated]

    @action(detail=True, methods=['POST'])
    def add_product(self, request, pk=None):
        data = json.loads(request.body)
        product_id = data['data']['product_id']
        product = ProductModel.objects.get(id=product_id)
        cart = self.get_object()
        try:
            cart_item = cart.cart_items.get(product = product)
            cart_item.count = cart_item.count + 1
            cart_item.save()
        except CartItemModel.DoesNotExist:
            cart_item = CartItemModel(product=product, count=1)
            cart_item.save()
            cart.cart_items.add(cart_item)
            cart.save()
        return Response({'status': 'add product to cart'})


    @action(detail=True, methods=['post'])
    def remove_product(self, request, pk=None):
        data = json.loads(request.body)
        cart_item_id = data['data']['cart_item_id']
        try:
            cart_item = CartItemModel.objects.get(id=cart_item_id)
            cart_item.delete()
        except CartItemModel.DoesNotExist:
            return Response({'status': 'not found product on cart'}, status=status.HTTP_404_NOT_FOUND)
        cart = self.get_object()
        serializer = CartSerializer(cart)
        return Response({'status': 'remove product on cart', 'cart': serializer.data})



    @action(detail=True, methods=['post'])
    def add_count(self, request, pk=None):
        data = json.loads(request.body)
        cart_item_id = data['data']['cart_item_id']
        try:
            cart_item = CartItemModel.objects.get(id=cart_item_id)
            cart_item.count += 1
            cart_item.save()
        except CartItemModel.DoesNotExist:
            return Response({'status': 'not found product on cart'}, status=status.HTTP_404_NOT_FOUND)

        cart = self.get_object()
        serializer = CartSerializer(cart)
        return Response({'status': 'remove product on cart', 'cart': serializer.data})

    @action(detail=True, methods=['post'])
    def remove_count(self, request, pk=None):
        data = json.loads(request.body)
        cart_item_id = data['data']['cart_item_id']
        try:
            cart_item = CartItemModel.objects.get(id=cart_item_id)
            if cart_item.count - 1 == 0:
                cart_item.delete()
                cart = self.get_object()
                serializer = CartSerializer(cart)
                return Response({'status': 'remove product on cart', 'cart': serializer.data})
            cart_item.count -= 1
            cart_item.save()
        except CartItemModel.DoesNotExist:
            return Response({'status': 'not found product on cart'}, status=status.HTTP_404_NOT_FOUND)

        cart = self.get_object()
        serializer = CartSerializer(cart)
        return Response({'status': 'remove product on cart', 'cart': serializer.data})

    @action(detail=True, methods=['post'])
    def create_order(self, request, pk=None):
        if not request.user.is_authenticated:
            return Response("forbiden", status=status.HTTP_401_UNAUTHORIZED)
        user_id = request.user.id
        cart = self.get_object()
        order = OrderModel(user_id=request.user.id)
        order.save()

        for i in cart.cart_items.all():
            product = ProductModel.objects.get(id=i.product.id)
            order_item = OrderItemModel(product=product, count=i.count)
            order_item.save()
            order.prdoucts.add(order_item)


        serializer = OrderSerializer(order)
        return Response({'status': 'create order', 'order': serializer.data})




